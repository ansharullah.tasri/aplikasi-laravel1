<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function index()
    {
        // $posts = DB::table('pertanyaan')->get();
        $posts = Pertanyaan::all();
        return view('pertanyaan.index', compact('posts'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            "judul" => 'required',
            "isi" => 'required'
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);


        return redirect('/pertanyaan');
    }

    public function show($id)
    {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();

        $post = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id)
    {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = Pertanyaan::find($id);
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            "judul" => 'required',
            "isi" => 'required'
        ]);
        // $query = DB::table('pertanyaan')->where('id', $id)->update([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi']
        // ]);
        $update = Pertanyaan::where('id', $id)->update([
                'judul' => $request['judul'],
                'isi' => $request['isi']
        ]);
        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan');
    }
}
