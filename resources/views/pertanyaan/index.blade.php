@extends('master')
@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Tabel Pertanyaan</h3>
    </div>
<div class="box-body">
    <a class="btn btn-info" href="/pertanyaan/create">Tambah Data</a>
    <table class="table table-bordered">
      <thead>
        <tr>
        <th style="width: 10px">#</th>
        <th>Judul</th>
        <th>Isi</th>
        <th style="width: 40px">Aksi</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($posts as $key => $post)
         <tr>
             <td>{{$key + 1}}</td>
             <td>{{$post -> judul}}</td>
             <td>{{$post -> isi}}</td>
             <td style="display: flex;">
                 <a style="margin-right: 5px;" href="/pertanyaan/{{$post->id}}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                 <a style="margin-right: 5px;" href="/pertanyaan/{{$post->id}}/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                 <form action="/pertanyaan/{{$post->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </form>
             </td>
         </tr>
         @empty
         <tr>
             <td colspan="4">Tidak ada data</td>
         </tr>
     @endforelse
    </tbody>

    </table>
  </div>
</div>
@endsection
