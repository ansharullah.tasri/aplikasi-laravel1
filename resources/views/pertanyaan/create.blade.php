@extends('master')
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Pertanyaan</h3>
    </div>
    <form role="form" method="POST" action="/pertanyaan">
        @csrf
      <div class="box-body">
        <a class="btn btn-warning" href="/pertanyaan">Kembali</a>
        <div class="form-group">
          <label for="judul">Judul</label>
        <input type="text" name="judul" class="form-control" id="judul" value="{{ old('judul', '')}}">
          @error('judul')
            <div class="alert alert-danger">{{ $message}}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">isi</label>
          <input type="text" name="isi" class="form-control" id="isi" value="{{ old('isi', '')}}">
          @error('isi')
            <div class="alert alert-danger">{{ $message}}</div>
          @enderror
        </div>

      </div>

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Tulis</button>
      </div>
    </form>
  </div>
@endsection
